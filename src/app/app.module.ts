import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FieldComponent} from './components/game/field/field.component';
import {GameboardComponent} from './components/game/gameboard/gameboard.component';
import {FieldverticalComponent} from './components/game/fieldvertical/fieldvertical.component';
import {FieldCornerComponent} from './components/game/field-corner/field-corner.component';
import {PropertyCardComponent} from './components/game/property-card/property-card.component';
import {RegisterComponent} from './components/account/register/register.component';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './components/account/login/login.component';
import {SocialLoginModule, AuthServiceConfig} from 'angular5-social-login';
import {getAuthServiceConfigs} from './socialloginConfig';
import {JwtInterceptor} from './security/JwtInterceptor';


import {LobbyComponent} from './components/menu/startgame/lobby/lobby.component';
import {InviteComponent} from './components/menu/startgame/invite/invite.component';
import {FriendsComponent} from './components/menu/startgame/invite/friends/friends.component';
import {RecentopponentsComponent} from './components/menu/startgame/invite/recentopponents/recentopponents.component';
import {EmailinvitationComponent} from './components/menu/startgame/invite/emailinvitation/emailinvitation.component';
import {FacebookgoogleComponent} from './components/menu/startgame/invite/facebookgoogle/facebookgoogle.component';
import {MenuComponent} from './components/menu/menu.component';
import {JoingameComponent} from './components/menu/joingame/joingame.component';
import {GamesettingsComponent} from './components/menu/startgame/gamesettings/gamesettings.component';
import {SettingsComponent} from './components/menu/settings/settings.component';
import {ActionBoxComponent} from './components/game/action-box/action-box.component';
import {MortgageComponent} from './components/game/gameboard/mortgage/mortgage.component';
import {UnmortgageComponent} from './components/game/gameboard/unmortgage/unmortgage.component';
import {BuildComponent} from './components/game/gameboard/build/build.component';
import {PawnboxComponent} from './components/game/pawnbox/pawnbox.component';
import {AcceptgameComponent} from './components/menu/joingame/acceptgame/acceptgame.component';
import {GameService} from './services/game.service';
import { RejoingameComponent } from './components/menu/joingame/rejoingame/rejoingame.component';
import {PlayerOverviewComponent} from './components/game/gameboard/player-overview/player-overview.component';

@NgModule({
  declarations: [
    AppComponent,
    FieldComponent,
    GameboardComponent,
    FieldverticalComponent,
    FieldCornerComponent,
    PropertyCardComponent,
    RegisterComponent,
    LoginComponent,
    LobbyComponent,
    InviteComponent,
    FriendsComponent,
    RecentopponentsComponent,
    EmailinvitationComponent,
    FacebookgoogleComponent,
    MenuComponent,
    GamesettingsComponent,
    JoingameComponent,
    SettingsComponent,
    ActionBoxComponent,
    MortgageComponent,
    UnmortgageComponent,
    BuildComponent,
    AcceptgameComponent,
    PawnboxComponent,
    AcceptgameComponent,
    RejoingameComponent,
    PlayerOverviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SocialLoginModule,
  ], exports: [
    PawnboxComponent
  ],
  providers: [
    {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs},
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    GameService
  ],
  bootstrap: [AppComponent]
})

export class AppModule {
}
