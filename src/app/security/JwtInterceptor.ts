import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import {UserService} from '../services/user.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  constructor(private userService: UserService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    if (this.userService.isLoggedIn()) {
      console.log('intercept jwt interceptor');
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ` + localStorage.getItem('id_token')
        }
      });
    }
    return next.handle(request);
  }
}
