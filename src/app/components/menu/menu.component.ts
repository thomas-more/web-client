import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../../services/game.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Player} from '../../model/player';
import {UserService} from '../../services/user.service';
import {Invite} from '../../model/invite';
import {Router} from '@angular/router';
import {User} from '../../model/user';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private gameService: GameService, private userService: UserService, private router: Router) { }
  private helper;
  private tokeninfo;
   invites: Invite[] = [];
  private username;
  ngOnInit() {
    console.log('test');
    this.helper = new JwtHelperService();
    this.tokeninfo = this.helper.decodeToken(localStorage.getItem('id_token'));
    this.username = this.tokeninfo.username;
    this.userService.getInvites(this.username).subscribe(res => {
      for (const invite of res) {
        this.invites.push(invite);
      }
    });
  }

  createLobby() {
    localStorage.setItem('gameId', null);
    this.gameService.createNewLobby(this.username);
  }

  logout() {
      this.userService.logoutUser();
      this.router.navigate(['/login']);
  }
}
