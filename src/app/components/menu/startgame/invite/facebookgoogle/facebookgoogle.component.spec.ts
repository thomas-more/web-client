import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacebookgoogleComponent } from './facebookgoogle.component';

describe('FacebookgoogleComponent', () => {
  let component: FacebookgoogleComponent;
  let fixture: ComponentFixture<FacebookgoogleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacebookgoogleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacebookgoogleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
