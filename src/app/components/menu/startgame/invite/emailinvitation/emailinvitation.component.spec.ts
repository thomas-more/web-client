import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailinvitationComponent } from './emailinvitation.component';

describe('EmailinvitationComponent', () => {
  let component: EmailinvitationComponent;
  let fixture: ComponentFixture<EmailinvitationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailinvitationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailinvitationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
