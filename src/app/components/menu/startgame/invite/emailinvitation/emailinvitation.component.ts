import { Component, OnInit } from '@angular/core';
import {GameService} from '../../../../../services/game.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-emailinvitation',
  templateUrl: './emailinvitation.component.html',
  styleUrls: ['./emailinvitation.component.css']
})
export class EmailinvitationComponent implements OnInit {

  constructor(private gameService: GameService, private router: Router) { }
  notFound = false;

  ngOnInit() {
    console.log('jqsdmfjsq');
  }


  invite(value: any) {
    this.notFound = false;
    this.gameService.inviteEmailPlayer(value).subscribe(
      data => console.log(data),
      err => this.notFound = true,
      () => {
        if (!this.notFound) {
          this.router.navigate(['/start']);
        }
      });
  }
}
