import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';


@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.css']
})
export class InviteComponent implements OnInit {

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  onClickFriends() {
    this.router.navigateByUrl('/invite/friends');
  }
  logout() {
    this.userService.logoutUser();
    this.router.navigate(['/login']);
  }
}
