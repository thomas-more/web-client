import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentopponentsComponent } from './recentopponents.component';

describe('RecentopponentsComponent', () => {
  let component: RecentopponentsComponent;
  let fixture: ComponentFixture<RecentopponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentopponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentopponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
