import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../../model/player';
import {GameService} from '../../../../services/game.service';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import {addPlayer} from '@angular/core/src/render3/players';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {UserService} from '../../../../services/user.service';

@Component({
  selector: 'app-lobby',
  templateUrl: './lobby.component.html',
  styleUrls: ['./lobby.component.css']
})
export class LobbyComponent implements OnInit {
  game;
  private _subscription;
  private helper;
  private tokeninfo;

  constructor(private gameService: GameService, private router: Router, private userService: UserService) {
    this.helper = new JwtHelperService();
    this.tokeninfo = this.helper.decodeToken(localStorage.getItem('id_token'));
    if (this.gameService.game != null) {
      this.game = this.gameService.game;
    }
  }

  ngOnInit() {
    this.gameService.connect();
    this._subscription = this.gameService.gameChange.subscribe((value) => {
      this.game = value;
      if (this.game.started) {
        this.router.navigate(['/play']);
      }
    });
  }

  startGame() {
    this.gameService.createNewGame();
  }

  pawnFree(pawn: string) {
    for (let p of this.game.players) {
      if (p.pawn === pawn) {
        return false;
      }
    }
    return true;
  }

  pickPawn(pawn: string) {
    this.gameService.pickPawn(pawn, this.game.players.find(p => p.name === this.tokeninfo.username).id);
  }

  logout() {
    this.userService.logoutUser();
    this.router.navigate(['/login']);
  }
}
