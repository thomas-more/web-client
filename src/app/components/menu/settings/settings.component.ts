import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user.service';
import {User} from '../../../model/user';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Observable} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {AuthService} from 'angular5-social-login';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  currentUser: User;
  settingsForm: FormGroup;
  registerError = false;
  changedUser = User;
  usernameTaken = false;

  constructor(private userService: UserService, private fb: FormBuilder, private socialAuthService: AuthService, private route: ActivatedRoute, private router: Router) {

  }

  ngOnInit() {
    this.settingsForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+\.[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+$/)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]]
    }, {validator: this.validatePasswords});


    const helper = new JwtHelperService();
    const tokeninfo = helper.decodeToken(localStorage.getItem('id_token'));
    console.log(tokeninfo);
    const username = tokeninfo.username;
    console.log(username);
    this.userService.getUser(username).subscribe(user => {
      this.currentUser = user;
      console.log(this.currentUser.password);
    });


  }


  onSubmit() {
    console.log('submit');
    const newuser = this.currentUser;
    newuser.username = this.username.value.toString();
    newuser.email = this.email.value.toString();
    newuser.password = this.password.value.toString();

    this.userService.updateUser(newuser).subscribe(user => {
      if (user == null) {
        this.usernameTaken = true;
      } else {
        this.usernameTaken = false;
        this.currentUser = user;

        this.socialAuthService.authState.subscribe(next => {
          if (next != null) {
            this.socialAuthService.signOut();
          }
        });
        this.userService.logoutUser();

        this.route.paramMap.pipe(switchMap((params: ParamMap) => this.userService.loginUser(newuser))).subscribe(
          (result) => {
            console.log(result);
            if (this.userService.isLoggedIn()) {
              this.router.navigate(['/menu']);
            } else {
            }
          }
          , error => {
            console.log(error as string);
          }
        );
      }
    });


  }

  validatePasswords(group: FormGroup) {
    return group.controls.password.value === group.controls.confirmPassword.value ? null : {notSame: true};
  }

  validateLengthPasswords(group: FormGroup) {
    console.log(group.controls.password.value);
    if (group.controls.password.value != null) {
      return group.controls.password.value.toString().length < 8 ? null : {tooShort: true};
    }
    return true;

  }


  get username() {
    return this.settingsForm.get('username');
  }

  get email() {
    return this.settingsForm.get('email');
  }

  get password() {
    return this.settingsForm.get('password');
  }

  get confirmPassword() {
    return this.settingsForm.get('confirmPassword');
  }

  toggleErrorMessage() {
    if (this.username.invalid) {
      this.usernameTaken = false;
    }
  }
  logout() {
    this.userService.logoutUser();
    this.router.navigate(['/login']);
  }

}
