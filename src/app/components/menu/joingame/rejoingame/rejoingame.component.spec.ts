import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejoingameComponent } from './rejoingame.component';

describe('RejoingameComponent', () => {
  let component: RejoingameComponent;
  let fixture: ComponentFixture<RejoingameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejoingameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejoingameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
