import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../../../../services/game.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-rejoingame',
  templateUrl: './rejoingame.component.html',
  styleUrls: ['./rejoingame.component.css']
})
export class RejoingameComponent implements OnInit {

  @Input() gameId;

  constructor(private gameService: GameService, private router: Router) { }

  ngOnInit() {
    console.log(this.gameId);
  }

  joinGame() {
    localStorage.setItem('gameId', this.gameId);
    this.router.navigate(['/play']);
  }
}
