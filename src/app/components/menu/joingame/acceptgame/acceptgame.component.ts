import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../../../../services/game.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-acceptgame',
  templateUrl: './acceptgame.component.html',
  styleUrls: ['./acceptgame.component.css']
})
export class AcceptgameComponent implements OnInit {

  @Input() gameId;
  @Input() userId;
  @Input() hostUser;
  constructor(private gameService: GameService) { }


  ngOnInit() {
  }

  acceptInvite() {
    this.gameService.connectWithId(this.userId, this.gameId);
  }
}
