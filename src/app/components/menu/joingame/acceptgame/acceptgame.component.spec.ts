import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptgameComponent } from './acceptgame.component';

describe('AcceptgameComponent', () => {
  let component: AcceptgameComponent;
  let fixture: ComponentFixture<AcceptgameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptgameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptgameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
