import { Component, OnInit } from '@angular/core';
import {Invite} from '../../../model/invite';
import {UserService} from '../../../services/user.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {GameService} from '../../../services/game.service';
import {User} from '../../../model/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-joingame',
  templateUrl: './joingame.component.html',
  styleUrls: ['./joingame.component.css']
})
export class JoingameComponent implements OnInit {

  constructor(private userService: UserService, private gameService: GameService, private router: Router) { }
  invites: Invite[] = [];
  private helper;
   tokeninfo;
  private username;
   user: User;
  ngOnInit() {
    this.helper = new JwtHelperService();
    this.tokeninfo = this.helper.decodeToken(localStorage.getItem('id_token'));
    this.username = this.tokeninfo.username;
    this.userService.getUser(this.username).subscribe(res => {console.log(res); this.user = res; });
    this.userService.getInvites(this.tokeninfo.username).subscribe(res => {
      for (const invite of res) {
        console.log(invite.hostUser);
        this.invites.push(invite);
      }
    });
  }

  logout() {
    this.userService.logoutUser();
    this.router.navigate(['/login']);
  }
}
