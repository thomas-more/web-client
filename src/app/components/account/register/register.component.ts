import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {User} from '../../../model/user';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  registerError = false;
  usernameTaken = false;
  private returnedUser: Object;
  constructor(private fb: FormBuilder, private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.fb.group({
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+\.[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+$/)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', [Validators.required]]
    }, {validator: this.validatePasswords});
  }

  validatePasswords(group: FormGroup) {
    return group.controls.password.value === group.controls.confirmPassword.value ? null : { notSame: true};
  }

  onSubmit() {
    const user = new User(this.username.value.toString(), this.email.value.toString(), this.password.value.toString());
    this.returnedUser = this.route.paramMap.pipe(switchMap((params: ParamMap) => this.userService.registerUser(user))).subscribe(
      (result) => {
        if (result === null) {
          this.usernameTaken = true;
        } else {
          this.router.navigate(['/login']);
        }
      }
      , error => {
        console.log(error as string);
      }
    );
  }

  get username() {
    return this.registerForm.get('username');
  }
  get email() {
    return this.registerForm.get('email');
  }
  get password() {
    return this.registerForm.get('password');
  }
  get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  toggleErrorMessage() {
    if (this.username.invalid) {
      this.usernameTaken = false;
    }
  }

  toLogin() {
    this.router.navigate(['/login']);
  }
}
