import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {User} from '../../../model/user';
import {switchMap} from 'rxjs/operators';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {
  AuthService,
  FacebookLoginProvider,
  GoogleLoginProvider
} from 'angular5-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  private returnedUser: Object;
  wrongCredentials = false;

  constructor(private fb: FormBuilder, private userService: UserService, private route: ActivatedRoute, private router: Router, private socialAuthService: AuthService) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  onSubmit() {
    const user = new User(this.username.value.toString(), '', this.password.value.toString());
    this.returnedUser = this.route.paramMap.pipe(switchMap((params: ParamMap) => this.userService.loginUser(user))).subscribe(
      (result) => {
        if (this.userService.isLoggedIn()) {
          this.router.navigate(['/menu']);
        } else {
          this.wrongCredentials = true;
        }
      }
      , error => {
        this.wrongCredentials = true;
        console.log(error as string);
      }
    );
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  public facebookLogin() {
    const socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    this.socialAuthService.signIn(socialPlatformProvider).then(
      (userData) => {
        this.route.paramMap.pipe(switchMap((params: ParamMap) => this.userService.loginSocialUser(userData))).subscribe(
          (result) => {
            if (this.userService.isLoggedIn()) {
              this.router.navigate(['/play']);
            }
          }
        );
      }
    );
  }

  logout() {
    this.socialAuthService.authState.subscribe(
      next => {
        if (next != null) {
          this.socialAuthService.signOut();
        }
      });
    this.userService.logoutUser();
  }
}
