import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../../../app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {AppComponent} from '../../../app.component';
import {FieldComponent} from '../../game/field/field.component';
import {GameboardComponent} from '../../game/gameboard/gameboard.component';
import {FieldverticalComponent} from '../../game/fieldvertical/fieldvertical.component';
import {FieldCornerComponent} from '../../game/field-corner/field-corner.component';
import {PropertyCardComponent} from '../../game/property-card/property-card.component';
import {RegisterComponent} from '../register/register.component';
import {LobbyComponent} from '../../menu/startgame/lobby/lobby.component';
import {InviteComponent} from '../../menu/startgame/invite/invite.component';
import {FriendsComponent} from '../../menu/startgame/invite/friends/friends.component';
import {RecentopponentsComponent} from '../../menu/startgame/invite/recentopponents/recentopponents.component';
import {EmailinvitationComponent} from '../../menu/startgame/invite/emailinvitation/emailinvitation.component';
import {FacebookgoogleComponent} from '../../menu/startgame/invite/facebookgoogle/facebookgoogle.component';
import {MenuComponent} from '../../menu/menu.component';
import {GamesettingsComponent} from '../../menu/startgame/gamesettings/gamesettings.component';
import {JoingameComponent} from '../../menu/joingame/joingame.component';
import {SettingsComponent} from '../../menu/settings/settings.component';
import {AuthServiceConfig, SocialLoginModule} from 'angular5-social-login';
import {getAuthServiceConfigs} from '../../../socialloginConfig';
import {JwtInterceptor} from '../../../security/JwtInterceptor';
import {PawnboxComponent} from '../../game/pawnbox/pawnbox.component';
import {UnmortgageComponent} from '../../game/gameboard/unmortgage/unmortgage.component';
import {MortgageComponent} from '../../game/gameboard/mortgage/mortgage.component';
import {BuildCommand} from '@angular/cli/commands/build-impl';
import {BuildComponent} from '../../game/gameboard/build/build.component';
import {ActionBoxComponent} from '../../game/action-box/action-box.component';
import {AcceptgameComponent} from '../../menu/joingame/acceptgame/acceptgame.component';
import {PlayerOverviewComponent} from '../../game/gameboard/player-overview/player-overview.component';
import {RejoingameComponent} from '../../menu/joingame/rejoingame/rejoingame.component';


describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        FieldComponent,
        GameboardComponent,
        FieldverticalComponent,
        FieldCornerComponent,
        PropertyCardComponent,
        RegisterComponent,
        LoginComponent,
        LobbyComponent,
        InviteComponent,
        FriendsComponent,
        RecentopponentsComponent,
        EmailinvitationComponent,
        FacebookgoogleComponent,
        MenuComponent,
        GamesettingsComponent,
        JoingameComponent,
        SettingsComponent,
        ActionBoxComponent,
        MortgageComponent,
        UnmortgageComponent,
        BuildComponent,
        AcceptgameComponent,
        PawnboxComponent,
        AcceptgameComponent,
        RejoingameComponent,
        PlayerOverviewComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        SocialLoginModule,
      ],
      providers: [
        {provide: AuthServiceConfig, useFactory: getAuthServiceConfigs},
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('loginform should be valid', () => {
    component.loginForm.controls['username'].setValue('naam');
    component.loginForm.controls['password'].setValue('password');
    expect(component.loginForm.valid).toBeTruthy();
  });

  it('registerform should not be valid, empty fields', () => {
    component.loginForm.controls['username'].setValue('');
    component.loginForm.controls['password'].setValue('');
    expect(component.loginForm.invalid).toBeTruthy();
  });
});
