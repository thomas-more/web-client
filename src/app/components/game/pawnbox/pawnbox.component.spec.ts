import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PawnboxComponent } from './pawnbox.component';

describe('PawnboxComponent', () => {
  let component: PawnboxComponent;
  let fixture: ComponentFixture<PawnboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PawnboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PawnboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
