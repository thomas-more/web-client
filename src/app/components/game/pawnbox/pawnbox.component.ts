import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../model/player';
import {Field} from '../../../model/field';

@Component({
  selector: 'app-pawnbox',
  templateUrl: './pawnbox.component.html',
  styleUrls: ['./pawnbox.component.css']
})
export class PawnboxComponent implements OnInit {

  @Input() public players: Player[];
  @Input() public field: Field;
  constructor() { }

  ngOnInit() {

  }

}
