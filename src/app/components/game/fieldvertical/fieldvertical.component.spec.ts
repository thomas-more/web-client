import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldverticalComponent } from './fieldvertical.component';

describe('FieldverticalComponent', () => {
  let component: FieldverticalComponent;
  let fixture: ComponentFixture<FieldverticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldverticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldverticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
