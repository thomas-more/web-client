import {Component, Input, OnInit} from '@angular/core';
import {Field} from '../../../model/field';
import {Player} from '../../../model/player';

@Component({
  selector: 'app-fieldvertical',
  templateUrl: './fieldvertical.component.html',
  styleUrls: ['./fieldvertical.component.css']
})
export class FieldverticalComponent implements OnInit {

  @Input() property: Field;
  @Input() players: Player[];
  constructor() { }

  ngOnInit() {
  }

}
