import {Component, EventEmitter, Input, NgModule, OnInit, Output} from '@angular/core';
import {Field} from '../../../model/field';
import {Player} from '../../../model/player';
import {PawnboxComponent} from '../pawnbox/pawnbox.component';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css']
})

export class FieldComponent implements OnInit {

  @Input() property: Field;
  @Input() players: Player[];
  @Input() rotationDegrees: Number;
  constructor() { }

  ngOnInit() {
  }

}
