import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldCornerComponent } from './field-corner.component';

describe('FieldCornerComponent', () => {
  let component: FieldCornerComponent;
  let fixture: ComponentFixture<FieldCornerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FieldCornerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FieldCornerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
