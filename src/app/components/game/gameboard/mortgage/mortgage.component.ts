import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Field} from '../../../../model/field';
import {Street} from '../../../../model/street';

@Component({
  selector: 'app-mortgage',
  templateUrl: './mortgage.component.html',
  styleUrls: ['./mortgage.component.css']
})
export class MortgageComponent implements OnInit {

  @Input() inputProperties: Field[];
  selectedField: Field;

  @Output() close = new EventEmitter<any>();

  onClose() {
    this.close.emit(true);
  }

  ngOnInit() {

  }

  setActive(inputProperty) {
    this.selectedField = inputProperty;
  }

}
