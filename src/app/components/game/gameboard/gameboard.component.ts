import {Component, OnInit} from '@angular/core';
import {Field} from '../../../model/field';
import {Player} from '../../../model/player';
import {GameService} from '../../../services/game.service';
import {Game} from '../../../model/game';
import {JwtHelperService} from '@auth0/angular-jwt';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.css']
})
export class GameboardComponent implements OnInit {
  test: Field[] = [];
  player: Player;
  game;
  hoverProperty: Field;
  mortgagebox: boolean;
  unmortgagebox: boolean;
  buildbox: boolean;
  private _subscription;
  private helper;
   tokeninfo;
  // player: Player = new Player(59, 'horse', 'Player1', 5, false, 500);
  ngOnInit() {
    this.gameService.connect();
    this._subscription = this.gameService.gameChange.subscribe((value) => {
      this.game = value;
      console.log(this.game);
      console.log('game aangekomen');
    });
    this.helper = new JwtHelperService();
    this.tokeninfo = this.helper.decodeToken(localStorage.getItem('id_token'));
    console.log('====================' + this.tokeninfo.username + '==============================');

  }

  constructor(
    private gameService: GameService, private userService: UserService, private router: Router
    ) {
    this.mortgagebox = false;
    this.unmortgagebox = false;
    this.buildbox = false;

    if (this.gameService.game != null) {
      this.game = this.gameService.game;
    }

  }


  logout() {
    this.userService.logoutUser();
    this.router.navigate(['/login']);
  }
  close() {
    this.mortgagebox = false;
    this.unmortgagebox = false;
    this.buildbox = false;
  }

  setHoverProperty(property: Field): void {
    if (this.mortgagebox === false) {
      console.log(property);
      this.hoverProperty = property;
    }

  }

  showMortgageBox() {
    this.hoverProperty = null;
    this.test.push(this.game.fields[14]);
    this.test.push(this.game.fields[8]);
    this.test.push(this.game.fields[18]);

    this.test.push(this.game.fields[16]);
    this.test.push(this.game.fields[6]);
    this.test.sort((a, b) => a.street.id - b.street.id);
    this.mortgagebox = true;
    this.unmortgagebox = false;
    this.buildbox = false;
  }


  showUnmortgageBox() {
    this.hoverProperty = null;
    this.test.push(this.game.fields[14]);
    this.test.push(this.game.fields[8]);
    this.test.push(this.game.fields[18]);
    this.test.sort((a, b) => a.street.id - b.street.id);
    this.unmortgagebox = true;
    this.mortgagebox = false;
    this.buildbox = false;
  }

  showBuildBox() {
    this.hoverProperty = null;
      this.test.push(this.game.fields[14]);
      this.test.push(this.game.fields[8]);
      this.test.push(this.game.fields[18]);
      console.log('testfield: ' + this.game.testField);
      this.test.sort((a, b) => a.street.id - b.street.id);
      this.unmortgagebox = false;
      this.mortgagebox = false;
      this.buildbox = true;
  }

  chat(msg: String) {
     this.gameService.sendMessage(this.game.playerPlaying.name, msg);
  }
}
