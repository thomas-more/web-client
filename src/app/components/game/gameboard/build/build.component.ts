import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Field} from '../../../../model/field';

@Component({
  selector: 'app-build',
  templateUrl: './build.component.html',
  styleUrls: ['./build.component.css']
})
export class BuildComponent implements OnInit {

  @Input() inputProperties: Field[];
  selectedField: Field;

  @Output() close = new EventEmitter<any>();
  onClose() {
    this.close.emit(true);
  }

  ngOnInit() {

  }

  addHouse(selectedField) {
    selectedField.housesAmount += 1;
  }

  setActive(inputProperty) {
    this.selectedField = inputProperty;
  }

}
