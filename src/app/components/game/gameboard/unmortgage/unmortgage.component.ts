import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Field} from '../../../../model/field';

@Component({
  selector: 'app-unmortgage',
  templateUrl: './unmortgage.component.html',
  styleUrls: ['./unmortgage.component.css']
})
export class UnmortgageComponent implements OnInit {

  @Input() inputProperties: Field[];

  selectedField: Field;

  @Output() close = new EventEmitter<any>();

  onClose() {
    this.close.emit(true);
  }

  ngOnInit() {

  }

  setActive(inputProperty) {
    this.selectedField = inputProperty;
  }

}
