import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnmortgageComponent } from './unmortgage.component';

describe('UnmortgageComponent', () => {
  let component: UnmortgageComponent;
  let fixture: ComponentFixture<UnmortgageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnmortgageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnmortgageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
});
