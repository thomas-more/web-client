import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../../model/player';
import {Field} from '../../../../model/field';

@Component({
  selector: 'app-player-overview',
  templateUrl: './player-overview.component.html',
  styleUrls: ['./player-overview.component.css']
})
export class PlayerOverviewComponent implements OnInit {

  constructor() {
  }

  @Input() player: Player;
  @Input() fields: Field[];

  ngOnInit() {
  }

}
