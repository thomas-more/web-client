import {Component, Input, OnInit} from '@angular/core';
import {Player} from '../../../model/player';
import {Observable} from 'rxjs';
import {GameService} from '../../../services/game.service';
import {Field} from '../../../model/field';

@Component({
  selector: 'app-action-box',
  templateUrl: './action-box.component.html',
  styleUrls: ['./action-box.component.css']
})
export class ActionBoxComponent implements OnInit {

  @Input() player: Player;
  @Input() fields: Field[];
  disabledButton: boolean;
  buyableProperty: boolean;

  constructor(
    private gameService: GameService,
  ) {
  }

  ngOnInit() {
    this.disabledButton = false;
    this.buyableProperty = false;
  }

  rollDice(): void {
    this.disabledButton = true;
    this.gameService.putRoll(this.player);
    this.buyableProperty = true;
  }

  endTurn(): void {
    this.gameService.endTurn();
  }

  buyProp(): void {
    this.gameService.buyProp();
    console.log(this.fields[this.player.position - 1]);
  }

}
