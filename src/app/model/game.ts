import {Player} from './player';

export class Game {
  id: number;
  started: boolean;
  players: Player[];
  parkingPot: number;
  playerPlaying: Player;
  maxTurnTime: number;
}
