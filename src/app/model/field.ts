import {Street} from './street';
import {Player} from './player';

export class Field {
  id: number;
  position: number;
  name: string;
  street: Street;
  buyValue: number;
  rentUnbuild: number;
  rentOneHouse: number;
  rentTwoHouses: number;
  rentThreeHouses: number;
  rentFourHouses: number;
  rentHotel: number;
  mortgage: number;
  priceHouseHotel: number;
  ownedBy: Player;
  housesAmount: number;
  priceOneUtility: number;
  priceTwoUtilities: number;
  priceOneStation: number;
  priceTwoStations: number;
  priceThreeStations: number;
  priceFourStations: number;
  inMortgage: boolean;
  gameId: number;
}

