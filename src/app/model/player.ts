export class Player {
  id: number;
  pawn: String;
  name: String;
  position: number;
  isInJail: boolean;
  money: number;
  gameId: number;

  constructor(id: number, pawn: String, name: String, position: number, isInJail: boolean, money: number) {
    this.id = id;
    this.pawn = pawn;
    this.name = name;
    this.position = position;
    this.isInJail = isInJail;
    this.money = money;
  }
}
