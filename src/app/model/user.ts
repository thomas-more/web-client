import {Player} from './player';

export class User {
  username;
  email;
  password;
  confirmed;
  players: Player[];
  constructor(username: string, email: string, password: string) {
    this.username = username;
    this.email = email;
    this.password = password;
  }
}
