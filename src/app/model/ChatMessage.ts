import {Street} from './street';

export class ChatMessage {
  chatMessage: String;
  player: String;
  gameId: number;

  constructor(chatMessage: String, player: String, gameId: number) {
    this.chatMessage = chatMessage;
    this.player = player;
    this.gameId = gameId;
  }

}
