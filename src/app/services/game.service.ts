import {Player} from '../model/player';
import {catchError} from 'rxjs/operators';
import {Field} from '../model/field';
import {Injectable} from '@angular/core';
import {User} from '../model/user';
import {BehaviorSubject, Observable, Subject, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'stompjs';
import 'rxjs/add/operator/do';
import {addPlayer} from '@angular/core/src/render3/players';
import {ChatMessage} from '../model/ChatMessage';
import {BASEURL} from '../Environnement';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class GameService {
  private gameUrl = BASEURL + '/api';
  game: any;
  gameChange: Subject<any> = new Subject<any>();
  ws: any;
  gameId;

  constructor(private http: HttpClient, private router: Router) {
    this.gameId = localStorage.getItem('gameId');
    if (this.gameId != null) {
      this.getGame(this.gameId).subscribe(res => {
        this.changeGame(res);
      });
    }
  }

  connect() {
    const socket = new SockJS(BASEURL + '/socket?jwt=' + localStorage.getItem('id_token'));
    this.ws = Stomp.over(socket);
    const that = this;
    this.ws.connect({}, function (frame) {
      that.ws.subscribe('/lobby' + that.gameId, function (message) {
        that.changeGame(JSON.parse(message.body));
      });
    }, function (error) {
      alert('STOMP error ' + error);
    });
  }

  connectWithId(userId: number, gameId: number) {
    localStorage.setItem('gameId', gameId.toString());
    this.gameId = gameId;
    const socket = new SockJS(BASEURL + '/socket?jwt=' + localStorage.getItem('id_token'));
    this.ws = Stomp.over(socket);
    const that = this;
    this.ws.connect({}, function (frame) {
    }, function (error) {
      alert('STOMP error ' + error);
    }, function (connect) {
      that.addPlayer(userId, gameId);
    });
  }

  public createNewGame() {
    console.log('starting the game!');
    this.ws.send('/api/game/create', {}, this.gameId);

  }

  getPlayers(gameId: String): Observable<Player[]> {
    return this.http.get<Player[]>(this.gameUrl + '/getplayersforgame/' + gameId, httpOptions)
      .do(res => console.log(res));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  getGame(gameId: number): any {
    return this.http.get(this.gameUrl + '/game/' + gameId, httpOptions).pipe();
  }

  createNewLobby(username: number) {
    return this.http.post(this.gameUrl + '/lobby/create', username, httpOptions).subscribe(res => {this.changeGame(res); this.router.navigate(['/start']); });
  }

  addPlayer(userId: number, gameId: number) {
    const obj = {userId, gameId};
    this.ws.send('/api/lobby/add', {}, JSON.stringify(obj));
    this.router.navigate(['/start']);
  }

  changeGame(game: any) {
    this.game = game;
    this.gameChange.next(this.game);
    localStorage.setItem('gameId', this.game.id);
    this.gameId = this.game.id;
    console.log(this.game);
  }

  inviteEmailPlayer(email: any) {
    const obj = {gameId: this.game.id, email};
    return this.http.post(this.gameUrl + '/lobby/invite', JSON.stringify(obj), httpOptions);
  }

  putRoll(player: Player) {
    const obj = {playerId: player.id, gameId: this.gameId};
    this.ws.send('/api/roll', {}, JSON.stringify(obj));
  }

  public sendMessage(chatMessage: String, player: String) {
    const playerMessage: ChatMessage =  new ChatMessage(player, chatMessage, this.gameId);
    this.ws.send('/api/newmessage', {}, JSON.stringify(playerMessage));
  }

  pickPawn(pawn: string, id: number) {
    const obj = {gameId: this.gameId, pawn: pawn, playerId: id};
    this.ws.send('/api/pickPawn', {}, JSON.stringify(obj));
  }
  endTurn() {
    this.ws.send('/api/endTurn', {}, JSON.stringify(this.gameId));
  }

  buyProp() {
    this.ws.send('/api/buyProperty', {}, JSON.stringify(this.gameId));
  }
}
