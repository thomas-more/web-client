import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import 'rxjs/add/operator/do';
import {User} from '../model/user';
import * as moment from 'moment';
import {Invite} from '../model/invite';
import {catchError} from 'rxjs/operators';
import {BASEURL} from '../Environnement';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public USERS_API = BASEURL + '/api';

  constructor(private http: HttpClient) {
  }

  public getInvites(username: string): Observable<Invite[]> {
    return this.http.get<Invite[]>(this.USERS_API + '/users/invites/' + username, httpOptions).pipe();
  }
  public registerUser(user: User): Observable<Object> {
    return this.http.post(this.USERS_API + '/users/register', JSON.stringify(user), httpOptions);
  }


  public updateUser(user: User): Observable<User> {
    return this.http.put<User>(this.USERS_API + '/users/update', JSON.stringify(user), httpOptions);
  }

  public getUser(username: String): Observable<User> {
    const url = `${this.USERS_API}/users/${username}`;
    return this.http.get<User>(url)
      .pipe(catchError(this.handleError<User>(`getUser username=${username}`)));
  }

  public loginUser(user: User): Observable<Object> {
    return this.http.post(this.USERS_API + '/users/login', JSON.stringify(user), httpOptions).do(res => this.setSession(res));
  }

  private setSession(authResult) {
    if (authResult != null) {
      const jwtData = authResult.accessToken.split('.')[1];
      const decodedJwtJsonData = window.atob(jwtData);
      const decodedJwtData = JSON.parse(decodedJwtJsonData);


      const exiresAt = moment().add(decodedJwtData.milliseconds, 'milliseconds');
      console.log(exiresAt.calendar());
      localStorage.setItem('id_token', authResult.accessToken);
      localStorage.setItem('expires_at', JSON.stringify(exiresAt.valueOf()));
    }
  }

  public logoutUser() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
  }

  public isLoggedIn() {
    console.log(moment().calendar());
    console.log(this.getExpiration().calendar());
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiration = localStorage.getItem('expires_at');
    const expiresAt = JSON.parse(expiration);
    return moment(expiresAt);
  }

  loginSocialUser(userData: Object): Observable<Object> {
    return this.http.post(this.USERS_API + '/users/sociallogin', JSON.stringify(userData), httpOptions).do(res => this.setSession(res));
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);
      return of(result as T);
    };
  }

}

