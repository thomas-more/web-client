import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegisterComponent} from './components/account/register/register.component';
import {LoginComponent} from './components/account/login/login.component';
import {GameboardComponent} from './components/game/gameboard/gameboard.component';
import {LobbyComponent} from './components/menu/startgame/lobby/lobby.component';
import {InviteComponent} from './components/menu/startgame/invite/invite.component';
import {FriendsComponent} from './components/menu/startgame/invite/friends/friends.component';
import {RecentopponentsComponent} from './components/menu/startgame/invite/recentopponents/recentopponents.component';
import {EmailinvitationComponent} from './components/menu/startgame/invite/emailinvitation/emailinvitation.component';
import {FacebookgoogleComponent} from './components/menu/startgame/invite/facebookgoogle/facebookgoogle.component';
import {MenuComponent} from './components/menu/menu.component';
import {JoingameComponent} from './components/menu/joingame/joingame.component';
import {SettingsComponent} from './components/menu/settings/settings.component';
import {GamesettingsComponent} from './components/menu/startgame/gamesettings/gamesettings.component';


const routes: Routes = [
  {path: 'register', component: RegisterComponent},
  {path: 'Webclient', component: LoginComponent},
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'play', component: GameboardComponent},
  {path: 'menu', component: MenuComponent},
  {path: 'start', component: LobbyComponent},
  {path: 'game-config', component: GamesettingsComponent},
  {path: 'invite', component: InviteComponent},
  {path: 'invite/friends', component: FriendsComponent},
  {path: 'invite/recent-opponents', component: RecentopponentsComponent},
  {path: 'invite/email-invitation', component: EmailinvitationComponent},
  {path: 'invite/facebook-google', component: FacebookgoogleComponent},
  {path: 'join', component: JoingameComponent},
  {path: 'settings', component: SettingsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
